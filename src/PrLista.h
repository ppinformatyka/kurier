#ifndef PRLISTA_H
#define PRLISTA_H

//Autor: Rafal Mikrut
template <class T>
class PrLista///Kazdy element przechowuje inforamcje o samym sobie o poprzednim elemencie i nastepnym
{
    PrLista<T>* poprzedni;
    PrLista<T>* nastepny;
    T* wartosc; // Nie wiem czy nie lepiej przez referencje

public:
    PrLista<T>(PrLista* = nullptr,T* = nullptr,PrLista* = nullptr);

    void UstawPoprzedniaListe(PrLista*);
    void UstawNastepnaListe(PrLista*);

    void UstawAktualny(T*);

    T* PokazAktualny();

    PrLista<T>* PokazPoprzedniaListe();
    PrLista<T>* PokazNastepnaListe();
};
///Konstruktor
template <class T>
PrLista<T>::PrLista(PrLista* po,T* wartosc ,PrLista* na)
{
    this->poprzedni = po;
    this->wartosc = wartosc;
    this->nastepny = na;
}
///Ustawianie poprzedniego i nastepnego elementu
template <class T>
void PrLista<T>::UstawPoprzedniaListe(PrLista<T>* po)
{
    poprzedni = po;
}

template <class T>
void PrLista<T>::UstawNastepnaListe(PrLista<T>* na)
{
    nastepny = na;
}

///Ustawia aktualny element
template <class T>
void PrLista<T>::UstawAktualny(T* wartosc)
{
    this->wartosc = wartosc;
}

///Pokazuje aktualny element
template <class T>
T* PrLista<T>::PokazAktualny()
{
    return wartosc;
}

///Pokazywanie poprzedniego i nastepnego elementu
template <class T>
PrLista<T>* PrLista<T>::PokazPoprzedniaListe()
{
    return poprzedni;
}

template <class T>
PrLista<T>* PrLista<T>::PokazNastepnaListe()
{
    return nastepny;
}

#endif
