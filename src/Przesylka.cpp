#include "Przesylka.h"

int Przesylka::ostatnieId = -1;

unsigned int Przesylka::getWaga() const {
    return waga;
}

unsigned int Przesylka::getCena() const {
    return cena;
}


string Przesylka::getTyp() const{
    switch(typ) {
        case PrzesylkaTyp::niebezpieczna:
            return string("Niebezpieczna");
        case PrzesylkaTyp::tlukliwa:
            return string("Tlukliwa");
        case PrzesylkaTyp::cenna:
            return string("Cenna");
        case PrzesylkaTyp::niezdefiniowana:
            return string("Niezdefiniowana");
        default:
            return string("Brak Informacji");
    }
}

PrzesylkaTyp Przesylka::getTyp(bool) const {
    return this->typ;
}

string Przesylka::getNazwa() const {
    return nazwa;
}

string Przesylka::getDataWyslania() const
{
    return dataWyslania;
}
string Przesylka::getDataDostarczenia() const
{
    return dataDostarczenia;
}
int Przesylka::getIdKuriera() const
{
    return idKuriera;
}


void Przesylka::setWaga(int waga) {
    this->waga = waga;
}

void Przesylka::setId(int id) {
    this->id = id;
}


void Przesylka::setNazwa(const string& nazwa) {
    this->nazwa = nazwa;
}

void Przesylka::setTyp(const PrzesylkaTyp typ) {
    this->typ = typ;
}

void Przesylka::setTyp(const string& typ) {
    if(typ == "niebezpieczna") {
        this->typ = PrzesylkaTyp::niebezpieczna;
    }
    else if(typ == "tlukliwa") {
        this->typ = PrzesylkaTyp::tlukliwa;
    }
    else if(typ == "cenna") {
        this->typ = PrzesylkaTyp::cenna;
    }
    else {
        this->typ = PrzesylkaTyp::niezdefiniowana;
    }
}

void Przesylka::setCena(int cena) {
    this->cena = cena;
}

bool Przesylka::operator==(const Przesylka& przesylkaPorownywana)
{
    if(getId() == przesylkaPorownywana.getId())
        return true;

    return false;
}

int Przesylka::getId() const
{
    return id;
}
int Przesylka::getIdOdbiorcy() const
{
    return idOdbiorcy;
}
int Przesylka::getIdNadawcy() const
{
    return idNadawcy;
}
void Przesylka::setIdNadawcy(int idNadawcy)
{
    this->idNadawcy = idNadawcy;
}
void Przesylka::setIdOdbiorcy(int idOdbiorcy)
{
    this->idOdbiorcy = idOdbiorcy;
}
void Przesylka::setDataWyslania(const string& dataWyslania)
{
    this->dataWyslania = dataWyslania;
}
void Przesylka::setDataDostarczenia(const string& dataDostarczenia)
{
    this->dataDostarczenia = dataDostarczenia;
}

void Przesylka::setIdKuriera(int idKuriera)
{
    this->idKuriera = idKuriera;
}
void Przesylka::WypiszDane()
{
    cout<<waga<<"\t"<<cena<<"\t"<<id<<"\t"<<idNadawcy<<"\t"<<idOdbiorcy<<"\t"<<nazwa<<"\t"<<getTyp()<<"\t"<<dataWyslania<<"\t"<<dataDostarczenia<<"\t"<<idKuriera<<endl;
}

int Przesylka::getOstatnieId()
{
    return ostatnieId;
}
void Przesylka::setOstatnieId(int _ostatnieId)
{
    ostatnieId = _ostatnieId;
}
