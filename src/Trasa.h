#ifndef TRASA_H
#define TRASA_H

#include "TransportTyp.h"
#include "Przesylka.h"

#include <string>

using namespace std;

//Autor Dawid Sobolewski
class Trasa {
public:
    Trasa(): zrodlo(string()), cel(string()), typ(TransportTyp::niezdefiniowany), odleglosc(0), cenaZaKm(0) {}
    Trasa(const string& zrodlo, const string& cel, TransportTyp typ, int odleglosc, int cenaZaKm = 2): zrodlo(zrodlo),
        cel(cel), typ(typ), odleglosc(odleglosc), cenaZaKm(cenaZaKm) {}
    ~Trasa() {}
    string getZrodlo() const;
    string getCel() const;
    unsigned int getOdleglosc() const;
    unsigned int getCalkowitaCena() const;
    float getCalkowitaCenaZPrzesylka(const Przesylka&) const;
    unsigned int getCzasTransportu() const;
    string getTransportTyp() const;
    void setZrodlo(const string&);
    void setCel(const string&);
    void setOdleglosc(int);
private:
    string zrodlo;
    string cel;
    TransportTyp typ;
    unsigned int odleglosc;
    unsigned int cenaZaKm;
};

#endif
