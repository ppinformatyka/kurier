#include "Magazyn.h"

Lista<Kurier> Magazyn::listaKurierow;
Lista<Klient> Magazyn::listaKlientow;
Lista<Administrator> Magazyn::listaAdministratorow;
Lista<Przesylka> Magazyn::listaPrzesylek;
Lista<Przesylka> Magazyn::listaPrzesylekPrzewozonych;
Lista<Przesylka> Magazyn::listaPrzesylekDostarczonych;


bool Magazyn::poczatek = 0;

void Magazyn::DodajKuriera(Kurier* kurierDodawany)
{
    Magazyn::listaKurierow.DodajNaKoncu(kurierDodawany);
    if(Czlowiek::getOstatnieId() < kurierDodawany->getId())
        Czlowiek::setOstatnieId(kurierDodawany->getId());
}
void Magazyn::DodajKlienta(Klient* klientDodawany)
{
    listaKlientow.DodajNaKoncu(klientDodawany);
    if(Czlowiek::getOstatnieId() < klientDodawany->getId())
        Czlowiek::setOstatnieId(klientDodawany->getId());
}
void Magazyn::DodajAdministratora(Administrator* administratorDodawany)
{
    listaAdministratorow.DodajNaKoncu(administratorDodawany);
    if(Czlowiek::getOstatnieId() < administratorDodawany->getId())
        Czlowiek::setOstatnieId(administratorDodawany->getId());
}
void Magazyn::DodajPrzesylke(Przesylka* przesylkaDodawana)
{
    listaPrzesylek.DodajNaKoncu(przesylkaDodawana);
    if(Przesylka::getOstatnieId() < przesylkaDodawana->getId())
        Przesylka::setOstatnieId(przesylkaDodawana->getId());
}
void Magazyn::DodajPrzesylkePrzewozona(Przesylka* przesylkaPrzewozonaDodawana)
{
    listaPrzesylekPrzewozonych.DodajNaKoncu(przesylkaPrzewozonaDodawana);
    if(Przesylka::getOstatnieId() < przesylkaPrzewozonaDodawana->getId())
        Przesylka::setOstatnieId(przesylkaPrzewozonaDodawana->getId());
}
void Magazyn::DodajPrzesylkeDostarczona(Przesylka* przesylkaDostarczonaDodawana)
{
    listaPrzesylekDostarczonych.DodajNaKoncu(przesylkaDostarczonaDodawana);
    if(Przesylka::getOstatnieId() < przesylkaDostarczonaDodawana->getId())
        Przesylka::setOstatnieId(przesylkaDostarczonaDodawana->getId());
}

bool Magazyn::CzyIstniejeUzytkownik(const string& login)
{
    listaKurierow.ResetujAktualny();
    while(listaKurierow.getAktualnaListe() != nullptr)
    {
        if(listaKurierow.getWartoscAktualny()->getLogin() == login)
        {
            return true;
        }
        listaKurierow.PrzejdzDoKolejnego();
    }
    listaKlientow.ResetujAktualny();
    while(listaKlientow.getAktualnaListe() != nullptr)
    {
        if(listaKlientow.getWartoscAktualny()->getLogin() == login)
        {
            return true;
        }
        listaKlientow.PrzejdzDoKolejnego();
    }
    listaAdministratorow.ResetujAktualny();
    while(listaAdministratorow.getAktualnaListe() != nullptr)
    {
        if(listaAdministratorow.getWartoscAktualny()->getLogin() == login)
        {
            return true;
        }
        listaAdministratorow.PrzejdzDoKolejnego();
    }
    return false;
}
string Magazyn::WyszukajHasloUzytkownika(const string& login)
{
    string haslo = "";
    listaKurierow.ResetujAktualny();
    while(listaKurierow.getAktualnaListe() != nullptr && haslo == "")
    {
        if(listaKurierow.getWartoscAktualny()->getLogin() == login)
        {
            haslo = listaKurierow.getWartoscAktualny()->getHaslo();
        }
        listaKurierow.PrzejdzDoKolejnego();
    }
    listaKlientow.ResetujAktualny();
    while(listaKlientow.getAktualnaListe() != nullptr && haslo == "")
    {
        if(listaKlientow.getWartoscAktualny()->getLogin() == login)
        {
            haslo = listaKlientow.getWartoscAktualny()->getHaslo();
        }
        listaKlientow.PrzejdzDoKolejnego();
    }
    listaAdministratorow.ResetujAktualny();
    while(listaAdministratorow.getAktualnaListe() != nullptr && haslo == "")
    {
        if(listaAdministratorow.getWartoscAktualny()->getLogin() == login)
        {
            haslo = listaAdministratorow.getWartoscAktualny()->getHaslo();
        }
        listaAdministratorow.PrzejdzDoKolejnego();
    }
    return haslo;
}
string Magazyn::WyszukajZawodUzytkownika(const string& login)
{
    string zawod = "";
    listaKurierow.ResetujAktualny();
    while(listaKurierow.getAktualnaListe() != nullptr && zawod == "")
    {
        if(listaKurierow.getWartoscAktualny()->getLogin() == login)
        {
            zawod = listaKurierow.getWartoscAktualny()->getTyp();
        }
        listaKurierow.PrzejdzDoKolejnego();
    }
    listaKlientow.ResetujAktualny();
    while(listaKlientow.getAktualnaListe() != nullptr && zawod == "")
    {
        if(listaKlientow.getWartoscAktualny()->getLogin() == login)
        {
            zawod = listaKlientow.getWartoscAktualny()->getTyp();
        }
        listaKlientow.PrzejdzDoKolejnego();
    }
    listaAdministratorow.ResetujAktualny();
    while(listaAdministratorow.getAktualnaListe() != nullptr && zawod == "")
    {
        if(listaAdministratorow.getWartoscAktualny()->getLogin() == login)
        {
            zawod = listaAdministratorow.getWartoscAktualny()->getTyp();
        }
        listaAdministratorow.PrzejdzDoKolejnego();
    }
    return zawod;
}
Kurier* Magazyn::ZwrocKuriera()
{
    if(poczatek == true)
    {
        listaKurierow.ResetujAktualny();
    }
    Kurier* temp = listaKurierow.getWartoscAktualny();
    if(temp != nullptr)
        listaKurierow.PrzejdzDoKolejnego();
    return temp;
}
Klient* Magazyn::ZwrocKlienta()
{
    if(poczatek == true)
    {
        listaKlientow.ResetujAktualny();
    }
    Klient* temp = listaKlientow.getWartoscAktualny();
    if(temp != nullptr)
        listaKlientow.PrzejdzDoKolejnego();
    return temp;
}
Administrator* Magazyn::ZwrocAdministratora()
{
    if(poczatek == true)
    {
        listaAdministratorow.ResetujAktualny();
    }
    Administrator* temp = listaAdministratorow.getWartoscAktualny();
    if(temp != nullptr)
        listaAdministratorow.PrzejdzDoKolejnego();
    return temp;
}
Przesylka* Magazyn::ZwrocPrzesylke()
{
    if(poczatek == true)
    {
        listaPrzesylek.ResetujAktualny();
    }
    Przesylka* temp = listaPrzesylek.getWartoscAktualny();
    if(temp != nullptr)
        listaPrzesylek.PrzejdzDoKolejnego();
    return temp;
}
Przesylka* Magazyn::ZwrocPrzesylkePrzewozona()
{
    if(poczatek == true)
    {
        listaPrzesylekPrzewozonych.ResetujAktualny();
    }
    Przesylka* temp = listaPrzesylekPrzewozonych.getWartoscAktualny();
    if(temp != nullptr)
        listaPrzesylekPrzewozonych.PrzejdzDoKolejnego();
    return temp;
}
Przesylka* Magazyn::ZwrocPrzesylkeDostarczona()
{
    if(poczatek == true)
    {
        listaPrzesylekDostarczonych.ResetujAktualny();
    }
    Przesylka* temp = listaPrzesylekDostarczonych.getWartoscAktualny();
    if(temp != nullptr)
        listaPrzesylekDostarczonych.PrzejdzDoKolejnego();
    return temp;
}
void Magazyn::setPoczatek(bool _poczatek)
{
    poczatek = _poczatek;
}
bool Magazyn::getPoczatek()
{
    return poczatek;
}
void* Magazyn::ZwrocSzukanegoUzytkownika(const string& login)
{
    listaKurierow.ResetujAktualny();
    while(listaKurierow.getAktualnaListe() != nullptr)
    {
        if(listaKurierow.getWartoscAktualny()->getLogin() == login)
        {
            return listaKurierow.getWartoscAktualny();
        }
        listaKurierow.PrzejdzDoKolejnego();
    }
    listaKlientow.ResetujAktualny();
    while(listaKlientow.getAktualnaListe() != nullptr)
    {
        if(listaKlientow.getWartoscAktualny()->getLogin() == login)
        {
            return listaKlientow.getWartoscAktualny();
        }
        listaKlientow.PrzejdzDoKolejnego();
    }
    listaAdministratorow.ResetujAktualny();
    while(listaAdministratorow.getAktualnaListe() != nullptr)
    {
        if(listaAdministratorow.getWartoscAktualny()->getLogin() == login)
        {
            return listaAdministratorow.getWartoscAktualny();
        }
        listaAdministratorow.PrzejdzDoKolejnego();
    }
    cout<<"Zwraca to null Magazyn::ZwrocSzukanegoUzytkownika";
    cin.get();
    return nullptr;
}
void Magazyn::DodajPieniadzeDoKlientow(int iloscKasy)
{
    listaKlientow.ResetujAktualny();
    while(listaKlientow.getAktualnaListe() != nullptr)
    {
        listaKlientow.getWartoscAktualny()->DodajKase(iloscKasy);

        listaKlientow.PrzejdzDoKolejnego();
    }
}
bool Magazyn::CzyIstniejeKlient(const int& id)
{
    listaKurierow.ResetujAktualny();
    while(listaKurierow.getAktualnaListe() != nullptr)
    {
        if(listaKurierow.getWartoscAktualny()->getId() == id)
        {
            return true;
        }
        listaKurierow.PrzejdzDoKolejnego();
    }
    return false;
}

void Magazyn::SprawdzCzyPaczkiDoszly()///Przesylka Przewozona -> Przesylka Dostarczona
{
    listaKurierow.ResetujAktualny();
    while(listaKurierow.getAktualnaListe() != nullptr)
    {
        listaPrzesylekPrzewozonych.ResetujAktualny();
        listaKurierow.getWartoscAktualny()->OdejmijDzien();
        if(listaKurierow.getWartoscAktualny()->getWTrasie() && listaKurierow.getWartoscAktualny()->getCzasDoZakonczeniaPodrozy() == 0)
        {
            while(listaPrzesylekPrzewozonych.getAktualnaListe() != nullptr)
            {
                if(listaPrzesylekPrzewozonych.getWartoscAktualny()->getIdKuriera() == listaKurierow.getWartoscAktualny()->getId())
                {
                    listaPrzesylekPrzewozonych.getWartoscAktualny()->WypiszDane();
                    listaKurierow.getWartoscAktualny()->setAktualnyUdzwig(0);
                    listaPrzesylekPrzewozonych.getWartoscAktualny()->setDataDostarczenia(Czas::getAktualnaData(false));
                    listaPrzesylekDostarczonych.DodajNaKoncu(listaPrzesylekPrzewozonych.getWartoscAktualny());
                    listaPrzesylekPrzewozonych.UsunElement(listaPrzesylekPrzewozonych.getAktualnaListe());
                    listaPrzesylekPrzewozonych.ResetujAktualny();
                    continue;
                }
                listaPrzesylekPrzewozonych.PrzejdzDoKolejnego();
            }
            listaKurierow.getWartoscAktualny()->setWTrasie(false);
        }
        listaKurierow.PrzejdzDoKolejnego();
    }
}
void Magazyn::PrzyporzadkujKurierowDoPrzesylek()///Przesylka -> Przesylka Przewozona
{
    listaKurierow.ResetujAktualny();
    while(listaKurierow.getAktualnaListe() != nullptr)
    {
        if(listaKurierow.getWartoscAktualny()->getWTrasie())
        {
            listaKurierow.PrzejdzDoKolejnego();
            continue;
        }
        listaPrzesylek.ResetujAktualny();
        while(listaPrzesylek.getAktualnaListe() != nullptr)
        {
            if(Kurier::maksymalnyUdzwig > listaKurierow.getWartoscAktualny()->getAktualnyUdzwig() + listaPrzesylek.getWartoscAktualny()->getWaga())
            {
                listaKurierow.getWartoscAktualny()->DodajDoUdzwigu(listaPrzesylek.getWartoscAktualny()->getWaga());
                listaPrzesylek.getWartoscAktualny()->setIdKuriera(listaKurierow.getWartoscAktualny()->getId());
                listaPrzesylekPrzewozonych.DodajNaKoncu(listaPrzesylek.getWartoscAktualny());
                listaPrzesylek.UsunElement(listaPrzesylek.getAktualnaListe());
                listaKurierow.getWartoscAktualny()->setCzasDoZakonczeniaPodrozy(2);///Dodaje czas dostarczenia przesylek - chwilowo ustawiony na sztywno
                listaKurierow.getWartoscAktualny()->setWTrasie(true);
                continue;
            }
            listaPrzesylek.PrzejdzDoKolejnego();
        }
        listaKurierow.PrzejdzDoKolejnego();
    }
}
void Magazyn::PrzeliczWagePrzesylekKurierow()
{
    listaPrzesylek.ResetujAktualny();
    while(listaPrzesylekPrzewozonych.getAktualnaListe() != nullptr)
    {
        listaKurierow.ResetujAktualny();
        while(listaKurierow.getAktualnaListe() != nullptr)
        {
            if(listaPrzesylekPrzewozonych.getWartoscAktualny()->getIdKuriera() == listaKurierow.getWartoscAktualny()->getId())
            {
                listaKurierow.getWartoscAktualny()->DodajDoUdzwigu(listaPrzesylekPrzewozonych.getWartoscAktualny()->getWaga());
                listaKurierow.getWartoscAktualny()->setCzasDoZakonczeniaPodrozy(4); /// Dane wpisane z pliku zawsze beda mialy 4 dni na dostarczenie
                listaKurierow.getWartoscAktualny()->setWTrasie(true);
                break;
            }
            listaKurierow.PrzejdzDoKolejnego();
        }
        listaPrzesylekPrzewozonych.PrzejdzDoKolejnego();
    }
}
void Magazyn::WyswietlWszystko()
{
    listaAdministratorow.ResetujAktualny();
    listaKlientow.ResetujAktualny();
    listaKurierow.ResetujAktualny();
    listaPrzesylek.ResetujAktualny();
    listaPrzesylekPrzewozonych.ResetujAktualny();
    listaPrzesylekDostarczonych.ResetujAktualny();
    cout<<endl<<"Czlowiek || login,haslo,id,typ,imie,nazwisko,kasa(Klient)/AktualnyUdzwig(Kurier),czasDoZakonczeniaPodrozy(Kurier)"<<endl;
    while(listaAdministratorow.getAktualnaListe() != nullptr)
    {
        listaAdministratorow.getWartoscAktualny()->WypiszDane();
        listaAdministratorow.PrzejdzDoKolejnego();
    }
    while(listaKlientow.getAktualnaListe() != nullptr)
    {
        listaKlientow.getWartoscAktualny()->WypiszDane();
        listaKlientow.PrzejdzDoKolejnego();
    }
    while(listaKurierow.getAktualnaListe() != nullptr)
    {
        listaKurierow.getWartoscAktualny()->WypiszDane();
        listaKurierow.PrzejdzDoKolejnego();
    }
    cout<<endl<<"Przesylka || waga,cena,id,idNadawcy,IdOdbiorcy,Nazwa,Typ,DataWyslania,DataDostarczenia,idKuriera"<<endl;
    while(listaPrzesylek.getAktualnaListe() != nullptr)
    {
        cout<<"Przesylka \t\t";
        listaPrzesylek.getWartoscAktualny()->WypiszDane();
        listaPrzesylek.PrzejdzDoKolejnego();
    }
    while(listaPrzesylekPrzewozonych.getAktualnaListe() != nullptr)
    {
        cout<<"Przesylka Przewozona \t";
        listaPrzesylekPrzewozonych.getWartoscAktualny()->WypiszDane();
        listaPrzesylekPrzewozonych.PrzejdzDoKolejnego();
    }
    while(listaPrzesylekDostarczonych.getAktualnaListe() != nullptr)
    {
        cout<<"Przesylka Dostarczona \t";
        listaPrzesylekDostarczonych.getWartoscAktualny()->WypiszDane();
        listaPrzesylekDostarczonych.PrzejdzDoKolejnego();
    }
}

void Magazyn::WyswietlPrzesylki(Klient* aktualnyKlient)
{
    listaPrzesylek.ResetujAktualny();
    listaPrzesylekPrzewozonych.ResetujAktualny();
    listaPrzesylekDostarczonych.ResetujAktualny();

    cout<<endl<<"Przesylka || waga,cena,id,idNadawcy,IdOdbiorcy,Nazwa,Typ,DataWyslania,DataDostarczenia,idKuriera"<<endl;
    while(listaPrzesylek.getAktualnaListe() != nullptr)
    {
        if(listaPrzesylek.getWartoscAktualny()->getIdNadawcy() == aktualnyKlient->getId())
        {
            cout<<"Przesylka\t\t";
            listaPrzesylek.getWartoscAktualny()->WypiszDane();
        }
        listaPrzesylek.PrzejdzDoKolejnego();
    }
    while(listaPrzesylekPrzewozonych.getAktualnaListe() != nullptr)
    {
        if(listaPrzesylekPrzewozonych.getWartoscAktualny()->getIdNadawcy() == aktualnyKlient->getId())
        {
            cout<<"Przesylka Przewozona\t";
            listaPrzesylekPrzewozonych.getWartoscAktualny()->WypiszDane();
        }
        listaPrzesylekPrzewozonych.PrzejdzDoKolejnego();
    }
    while(listaPrzesylekDostarczonych.getAktualnaListe() != nullptr)
    {
        if(listaPrzesylekDostarczonych.getWartoscAktualny()->getIdNadawcy() == aktualnyKlient->getId())
        {
            cout<<"Przesylka Dostarczona\t";
            listaPrzesylekDostarczonych.getWartoscAktualny()->WypiszDane();
        }
        listaPrzesylekDostarczonych.PrzejdzDoKolejnego();
    }
}
void Magazyn::WyswietlPrzesylki(Kurier* aktualnyKurier)
{
    listaPrzesylekPrzewozonych.ResetujAktualny();

    cout<<endl<<"Przesylka || waga,cena,id,idNadawcy,IdOdbiorcy,Nazwa,Typ,DataWyslania,DataDostarczenia,idKuriera"<<endl;
    while(listaPrzesylekPrzewozonych.getAktualnaListe() != nullptr)
    {
        if(listaPrzesylekPrzewozonych.getWartoscAktualny()->getIdKuriera() == aktualnyKurier->getId())
        {
            cout<<"Przesylka Przewozona\t";
            listaPrzesylekPrzewozonych.getWartoscAktualny()->WypiszDane();
        }
        listaPrzesylekPrzewozonych.PrzejdzDoKolejnego();
    }
}
bool Magazyn::WywalJakasPrzesylke(int idKuriera,int idPrzesylki)
{
    listaPrzesylekPrzewozonych.ResetujAktualny();

    while(listaPrzesylekPrzewozonych.getAktualnaListe() != nullptr)
    {
        if(listaPrzesylekPrzewozonych.getWartoscAktualny()->getId() == idPrzesylki && listaPrzesylekPrzewozonych.getWartoscAktualny()->getIdKuriera() == idKuriera)
        {
            listaPrzesylekPrzewozonych.UsunElement(listaPrzesylekPrzewozonych.getAktualnaListe());
            return true;
        }
        listaPrzesylekPrzewozonych.PrzejdzDoKolejnego();
    }
    return false;
}
bool Magazyn::UsunUzytkownika(string login)
{
    listaAdministratorow.ResetujAktualny();
    while(listaAdministratorow.getAktualnaListe() != nullptr)
    {
        if(listaAdministratorow.getWartoscAktualny()->getLogin() == login)
        {
            listaAdministratorow.UsunElement(listaAdministratorow.getAktualnaListe());
            return true;
        }
        listaAdministratorow.PrzejdzDoKolejnego();
    }
    listaKlientow.ResetujAktualny();
    while(listaKlientow.getAktualnaListe() != nullptr)
    {
        if(listaKlientow.getWartoscAktualny()->getLogin() == login)
        {
            listaKlientow.UsunElement(listaKlientow.getAktualnaListe());
            return true;
        }
        listaKlientow.PrzejdzDoKolejnego();
    }
    listaKurierow.ResetujAktualny();
    while(listaKurierow.getAktualnaListe() != nullptr)
    {
        if(listaKurierow.getWartoscAktualny()->getLogin() == login)
        {
            listaKurierow.UsunElement(listaKurierow.getAktualnaListe());
            return true;
        }
        listaKurierow.PrzejdzDoKolejnego();
    }
    return false;
}
