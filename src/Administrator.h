#ifndef ADMINISTRATOR_H
#define ADMINISTRATOR_H

#include "Czlowiek.h"

#include<iostream>
#include<string>

using namespace std;

//Autor: Rafal Mikrut
class Administrator : public Czlowiek
{
public:
    Administrator(string,string,int,string,string,string);
    void WyswietlPanel();
};

#endif
