#ifndef PRZESYLKA_H
#define PRZESYLKA_H

#include "TransportTyp.h"
#include "PrzesylkaTyp.h"

#include <string>
#include <iostream>
using namespace std;

//Autor Dawid Sobolewski
class Przesylka {
public:
    Przesylka(): waga(-1), cena(-1), id(-1), idNadawcy(-1),idOdbiorcy(-1),nazwa(""), typ(PrzesylkaTyp::niezdefiniowana),
                dataWyslania("0"),dataDostarczenia("0"),idKuriera(-1){}
    Przesylka(const unsigned int waga, const unsigned int cena, const int id,
             const int idNadawcy, const int idOdbiorcy, const string& nazwa, const PrzesylkaTyp& typ,
             const string& dataWyslania, const string& dataDostarczenia, const int idKuriera) :
                waga(waga), cena(cena),id(id),idNadawcy(idNadawcy), idOdbiorcy(idOdbiorcy), nazwa(nazwa), typ(typ),
                dataWyslania(dataWyslania), dataDostarczenia(dataDostarczenia), idKuriera(idKuriera) {}
    ~Przesylka() {}

    unsigned int getWaga() const;
    unsigned int getKod() const;
    unsigned int getCena() const;
    int getIdNadawcy() const;
    int getId() const;
    int getIdOdbiorcy() const;
    string getTyp() const;
    PrzesylkaTyp getTyp(bool) const;
    string getNazwa() const;
    static int getOstatnieId();
    string getDataWyslania() const;
    string getDataDostarczenia() const;
    int getIdKuriera() const;




    void setWaga(int);
    void setCena(int);
    void setId(int);
    void setIdNadawcy(int);
    void setIdOdbiorcy(int);
    void setNazwa(const string&);
    void setTyp(const PrzesylkaTyp);
    void setTyp(const string&);
    static void setOstatnieId(int);
    void setDataWyslania(const string&);
    void setDataDostarczenia(const string&);
    void setIdKuriera(int);

    bool operator==(const Przesylka&);
    void WypiszDane();

private:
    unsigned int waga;
    unsigned int cena;
    int id;
    int idNadawcy;
    int idOdbiorcy;
    string nazwa;
    PrzesylkaTyp typ;
    static int ostatnieId;
    string dataWyslania;
    string dataDostarczenia;
    int idKuriera;
};
#endif
