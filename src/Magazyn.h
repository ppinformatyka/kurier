#ifndef MAGAZYN_H
#define MAGAZYN_H

#include "PrLista.h"
#include "Kurier.h"
#include "Klient.h"
#include "Administrator.h"
#include "Przesylka.h"
#include "Lista.h"
#include "Czas.h"

#include <iostream>

using namespace std;

//Autor: Rafal Mikrut
using namespace std;

class Magazyn
{
    static Lista<Kurier> listaKurierow;
    static Lista<Klient> listaKlientow;
    static Lista<Administrator> listaAdministratorow;
    static Lista<Przesylka> listaPrzesylek;
    static Lista<Przesylka> listaPrzesylekPrzewozonych;
    static Lista<Przesylka> listaPrzesylekDostarczonych;

    static bool poczatek;
public:
    static void DodajKuriera(Kurier*);
    static void DodajKlienta(Klient*);
    static void DodajAdministratora(Administrator*);

    static void DodajPrzesylke(Przesylka*);
    static void DodajPrzesylkePrzewozona(Przesylka*);
    static void DodajPrzesylkeDostarczona(Przesylka*);

    static Kurier* ZwrocKuriera();
    static Klient* ZwrocKlienta();
    static Administrator* ZwrocAdministratora();
    static Przesylka* ZwrocPrzesylke();
    static Przesylka* ZwrocPrzesylkePrzewozona();
    static Przesylka* ZwrocPrzesylkeDostarczona();

    static bool CzyIstniejeUzytkownik(const string&);
    static string WyszukajHasloUzytkownika(const string&);
    static string WyszukajZawodUzytkownika(const string&);
    static void* ZwrocSzukanegoUzytkownika(const string&);
    static bool CzyIstniejeKlient(const int&);

    static void SprawdzCzyPaczkiDoszly();
    static void PrzyporzadkujKurierowDoPrzesylek();
    static void PrzeliczWagePrzesylekKurierow();

    static void DodajPieniadzeDoKlientow(int);

    static void UsunUzytkownika(int);

    static void WyswietlPrzesylki(Kurier*);
    static void WyswietlPrzesylki(Klient*);
    static void WyswietlPrzesylki();

    static bool UsunUzytkownika(string);
    static void WyswietlWszystko();

    static bool getPoczatek();
    static void setPoczatek(bool);

    static bool WywalJakasPrzesylke(int,int);
};
#endif
