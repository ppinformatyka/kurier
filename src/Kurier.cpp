#include "Kurier.h"

int Kurier::maksymalnyUdzwig = 200;

Kurier::Kurier(string login,string haslo,int id, string typ, string imie,string nazwisko, int czasDoZakonczeniaPodrozy):Czlowiek(login,haslo,id,typ,imie,nazwisko)
{
    this->czasDoZakonczeniaPodrozy = czasDoZakonczeniaPodrozy;
    if(this->czasDoZakonczeniaPodrozy > 0) this->wTrasie = false;
    this->aktualnyUdzwig = 0;
}

Pozycja Kurier::getPozycja() const {
    return this->pozycja;
}

int Kurier::getCzasDoZakonczeniaPodrozy()
{
    return czasDoZakonczeniaPodrozy;
}
void Kurier::setCzasDoZakonczeniaPodrozy(int czasDoZakonczeniaPodrozy)
{
    this->czasDoZakonczeniaPodrozy = czasDoZakonczeniaPodrozy;
}

void Kurier::OdejmijDzien()
{
    if(czasDoZakonczeniaPodrozy>0)
        this->czasDoZakonczeniaPodrozy--;
}

int Kurier::getAktualnyUdzwig()
{
    return aktualnyUdzwig;
}

bool Kurier::getWTrasie()
{
    return wTrasie;
}

void Kurier::setWTrasie(bool wTrasie)
{
    this->wTrasie = wTrasie;
}
void Kurier::setAktualnyUdzwig(int aktualnyUdzwig)
{
    this->aktualnyUdzwig = aktualnyUdzwig;
}

void Kurier::DodajDoUdzwigu(int dodawanyUdzwig)
{
    this->aktualnyUdzwig += dodawanyUdzwig;
}
void Kurier::WypiszDane()
{
    cout<<login+"\t"+haslo+"\t"+to_string(id)+"\t"+typ+"\t"+imie+"\t"+nazwisko+"\t"+to_string(aktualnyUdzwig)+"\t"+to_string(czasDoZakonczeniaPodrozy)<<endl;
}

