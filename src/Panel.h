#ifndef PANEL_H
#define PANEL_H

#include "Klient.h"
#include "Kurier.h"
#include "Administrator.h"
#include "Magazyn.h"
#include "Czas.h"

#include<iostream>

using namespace std;

//Autor: Rafal Mikrut
class Panel
{
public:
    Panel(Kurier*);
    void WyswietlPanel(Kurier*);
    void WyswietlPrzewozonePrzesylki(Kurier*);
    void WywalJakasPrzesylke(Kurier*);

    Panel(Administrator*);
    void WyswietlPanel(Administrator*);
    void WyswietlWszystko(Administrator*);
    void UsunUzytkownika(Administrator*);

    Panel(Klient*);
    void WypiszWyslanePrzesylki(Klient*);
    void WyslijPrzesylke(Klient*);
    void AnulujPrzesylke(Klient*);
    void SprawdzWyslanePrzesylki(Klient*);
    void WyswietlPanel(Klient*);
};

#endif
