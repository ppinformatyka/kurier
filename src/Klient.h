#ifndef KLIENT_H
#define KLIENT_H

#include "Czlowiek.h"
#include "Pozycja.h"
#include "Przesylka.h"

#include <iostream>
#include <string>
#include <stdio.h>

using namespace std;

//Autor: Rafal Mikrut
class Klient : public Czlowiek
{
    Pozycja pozycja;
    int kasa;
public:
    Klient(string,string,int,string,string,string,int);
    void WypiszWyslanePrzesylki();
    void WyslijPrzesylke();
    void WyswietlPanel();

    void setKasa(int);
    int getKasa();

    void DodajKase(int);
    void AnulujPrzesylke();
    void SprawdzWyslanePrzesylki();
    void WypiszDane();
};
#endif
