#ifndef LISTA_H
#define LISTA_H

#include "PrLista.h"

#include<iostream>

using namespace std;

//Autor: Rafal Mikrut
template <class T>///Szablon jesli potrzeba to tworzy klase dowolnego typu T np. int,float czy nawet Kurier
class Lista///Zawiera wskaznik do poczatku i konca listy, moze rowniez
{

    PrLista<T>* poczatek;
    PrLista<T>* aktualny;
    PrLista<T>* koniec;

public:
    Lista<T>();

    void DodajNaKoncu(T*);

    T* getWartoscPoczatek();
    T* getWartoscAktualny();
    T* getWartoscKoniec();

    PrLista<T>* getPoczatekListy();
    PrLista<T>* getAktualnaListe();
    PrLista<T>* getKoniecListy();

    void UsunElement(PrLista<T>*);

    void ResetujAktualny();
    void PrzejdzDoKolejnego();
    ///Sortowanie raczej niepotrzebne i raczej niemozliwe dla szablonu klasy
};

template <class T>
Lista<T>::Lista()
{
    this->poczatek = nullptr;
    this->aktualny = nullptr;
    this->koniec = nullptr;
}

template <class T>
void Lista<T>::DodajNaKoncu(T* naKoniec)
{
    if (poczatek == nullptr && koniec == nullptr)
    {
        koniec = new PrLista<T>(nullptr,naKoniec,nullptr);
        poczatek = koniec;
        aktualny = koniec;
    }
    else if (poczatek != nullptr && koniec != nullptr)
    {
        PrLista<T>* nowyKoniec = new PrLista<T>(koniec,naKoniec,nullptr);
        koniec->UstawNastepnaListe(nowyKoniec);
        koniec = nowyKoniec;
    }
    else
    {
        cout<<"To nie powinno sie uruchomic - Lista::DodajNaKoncu";
        cin.get();
    }
}

template <class T>
PrLista<T>* Lista<T>::getPoczatekListy()
{
    return poczatek;
}
template <class T>
PrLista<T>* Lista<T>::getAktualnaListe()
{
    return aktualny;
}
template <class T>
PrLista<T>* Lista<T>::getKoniecListy()
{
    return koniec;
}

template <class T>
T* Lista<T>::getWartoscPoczatek()
{
    if(poczatek == nullptr)
        return nullptr;
    return poczatek->PokazAktualny();
}
template <class T>
T* Lista<T>::getWartoscAktualny()
{
    if(aktualny == nullptr)
        return nullptr;
    return aktualny->PokazAktualny();
}
template <class T>
T* Lista<T>::getWartoscKoniec()
{
    if(koniec == nullptr)
        return nullptr;
    return koniec->PokazAktualny();
}

template <class T>
void Lista<T>::ResetujAktualny()
{
    aktualny = poczatek;
}

template <class T>
void Lista<T>::UsunElement(PrLista<T>* ktory)
{
    ResetujAktualny();
    while(aktualny != nullptr)
    {
        if(aktualny == ktory)
        {
            if(aktualny == poczatek && aktualny == koniec)
            {
                poczatek = nullptr;
                koniec = nullptr;
            }
            else if(aktualny != poczatek && aktualny == koniec)
            {
                aktualny->PokazPoprzedniaListe()->UstawNastepnaListe(nullptr);
                koniec = aktualny->PokazPoprzedniaListe();
            }
            else if(aktualny == poczatek && aktualny != koniec)
            {
                aktualny->PokazNastepnaListe()->UstawPoprzedniaListe(nullptr);
                poczatek = aktualny->PokazNastepnaListe();
            }
            else if(aktualny != poczatek && aktualny != koniec)
            {
                aktualny->PokazNastepnaListe()->UstawPoprzedniaListe(aktualny->PokazPoprzedniaListe());
                aktualny->PokazPoprzedniaListe()->UstawNastepnaListe(aktualny->PokazNastepnaListe());
                poczatek = aktualny->PokazNastepnaListe();
            }
            ResetujAktualny();
            return ;
        }
        PrzejdzDoKolejnego();
    }
    ResetujAktualny();
}

template <class T>
void Lista<T>::PrzejdzDoKolejnego()
{
    if(aktualny != koniec)
        aktualny = aktualny->PokazNastepnaListe();
    else
        aktualny = nullptr;
}
#endif
