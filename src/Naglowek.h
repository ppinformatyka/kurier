#ifndef NAGLOWEK_H
#define NAGLOWEK_H

#include <iostream>

#include "Czas.h"
#include "Czlowiek.h"

using namespace std;

//Autor: Dawid Sobolewski
class Naglowek
{
    public:
    static void RysujNaglowek(Czlowiek*);
};

#endif
