#ifndef KURIER_H
#define KURIER_H

#include "Czlowiek.h"
#include "Pozycja.h"
#include "Naglowek.h"

#include<iostream>
#include<string>

using namespace std;

//Autor: Rafal Mikrut
class Kurier : public Czlowiek
{
protected:
    int czasDoZakonczeniaPodrozy;
    bool wTrasie;
    Pozycja pozycja;
    int aktualnyUdzwig;

public:
    Kurier(string,string,int,string,string,string,int);
    Pozycja getPozycja() const;

    int getCzasDoZakonczeniaPodrozy();
    void setCzasDoZakonczeniaPodrozy(int);

    void OdejmijDzien();
    int getAktualnyUdzwig();
    void setAktualnyUdzwig(int);
    void DodajDoUdzwigu(int);

    void setWTrasie(bool);
    bool getWTrasie();

    void WypiszDane();

    static int maksymalnyUdzwig;
};
#endif
