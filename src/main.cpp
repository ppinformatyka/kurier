#include <iostream>
#include <cstdlib> //Do system ("cls") - czysci konsole
#include <fstream>
#include <string>

using namespace std;

#include "Czas.h"
#include "Logowanie.h"
#include "Kurier.h"
#include "Klient.h"
#include "Administrator.h"
#include "Panel.h"
#include "Naglowek.h"
#include "Magazyn.h"

//Autor Rafal Mikrut
int main(int argc, char** argv)
{

    Czas::Inicjalizuj();
    Logowanie::Inicjalizuj();
    ///Test Szyfrowania i deszyfrowania
    /*
    string tekst = "1";
    string tekst2 = tekst;

    Logowanie::Szyfrowanie(tekst);
    cout<<tekst;
    Logowanie::Deszyfrowanie(tekst2);
    cout<<tekst2;
    */
    ///TEST CZASU
    /*for(int i=1;i<400;i++)
    {
        Czas::getAktualnaData();
        Czas::KolejnyDzien();
        if(i%7 == 0)
            cout<<endl;
        else
            cout<<"\t";
    }
    */

    char wybor;
    while(true)///Przechodzi dzien
    {
        try
        {
            while(true)///Rejestracja, Logowanie, Panel Użytkownikow
            {
                try
                {
                    Naglowek::RysujNaglowek(nullptr);

                    //Magazyn::WyswietlWszystko();
                    cout<<"Wpisz 1 by przejsc do logowania,2 do rejestracji, 3 by przejsc do kolejnego dnia, 0 by wyjsc z programu: ";
                    cin>>wybor;
                    cin.ignore(INT_MAX,'\n'); /// Czysci cin by nie bylo w nim niepotrzebnych znakow
                    if(wybor == '1')///Logowanie
                    {
                        void* zalogowany = Logowanie::Zaloguj();
                        if(Logowanie::JakiUzytkownik() == "Klient")
                        {
                            Klient* zalogowanyKlient = reinterpret_cast<Klient*>(zalogowany);

                            Panel panelUzytkownika(zalogowanyKlient);
                        }
                        if(Logowanie::JakiUzytkownik() == "Kurier")
                        {
                            Kurier* zalogowanyKurier = reinterpret_cast<Kurier*>(zalogowany);

                            Panel panelUzytkownika(zalogowanyKurier);
                        }
                        if(Logowanie::JakiUzytkownik() == "Administrator")
                        {
                            Administrator* zalogowanyAdministrator = reinterpret_cast<Administrator*>(zalogowany);

                            Panel panelUzytkownika(zalogowanyAdministrator);
                        }
                    }
                    else if (wybor == '2')///Rejestracja
                    {
                        Logowanie::Rejestruj();
                    }
                    else if (wybor == '3') break;///Kolejny dzien
                    else if (wybor == '0') throw(int());///Wyjscie
                }
                catch(float)
                {
                    continue;
                }
            }///KONIEC Rejestracja, Logowanie, Panel Użytkownikow
        }
        catch(int)
        {
            ///Wyjscie z programu
            cout<<"Wylaczanie programu";
            break;
        }
        Magazyn::PrzyporzadkujKurierowDoPrzesylek();
        Czas::KolejnyDzien();
        Magazyn::DodajPieniadzeDoKlientow(160);
        Magazyn::SprawdzCzyPaczkiDoszly();
    }///KONIEC Przechodzi dzien
    Logowanie::ZapisywanieDanych();
    cin.get();//Czeka by nie zamknął się nieoczekiwanie program
    return 0;
}

