#include "Czas.h"

int Czas::dzienPoczatkowy=17;
int Czas::miesiacPoczatkowy=06;
int Czas::rokPoczatkowy=2018;

int Czas::dzienMiesiaca=Czas::dzienPoczatkowy;
int Czas::miesiacRoku=Czas::miesiacPoczatkowy;
int Czas::rok=Czas::rokPoczatkowy;

int Czas::poczatkowePrzesuniecieDnia;

int Czas::dzien=1;

int Czas::liczbaDniZwykla[12];

string Czas::aktualnaData = to_string(Czas::dzienMiesiaca) + "-" + to_string(Czas::miesiacRoku) + "-" + to_string(Czas::rok);

void Czas::Inicjalizuj()
{
    Czas::liczbaDniZwykla[0] = 31;
    Czas::liczbaDniZwykla[1] = 28;
    Czas::liczbaDniZwykla[2] = 31;
    Czas::liczbaDniZwykla[3] = 30;
    Czas::liczbaDniZwykla[4] = 31;
    Czas::liczbaDniZwykla[5] = 30;
    Czas::liczbaDniZwykla[6] = 31;
    Czas::liczbaDniZwykla[7] = 31;
    Czas::liczbaDniZwykla[8] = 30;
    Czas::liczbaDniZwykla[9] = 31;
    Czas::liczbaDniZwykla[10] = 30;
    Czas::liczbaDniZwykla[11] = 31;
    ///Poczatkowe przesuniecie dnia - chodzi o to by wiedziec ktorym dniem w roku byla poczatkowa data np 15 styczen byl pietnastym dniem i o tyle trzeba przesuwac wszystkie obliczenia dat
    poczatkowePrzesuniecieDnia = 0;

    for(int i=1; i<miesiacPoczatkowy;i++)
    {
        if(Czas::rok % 4 != 0 || i != 1)
        {
            Czas::poczatkowePrzesuniecieDnia += liczbaDniZwykla[i-1];
        }
        else
        {
            Czas::poczatkowePrzesuniecieDnia += 29; ///Luty i dzien przestępny
        }
    }
    poczatkowePrzesuniecieDnia += dzienPoczatkowy - 1;
    Czas::ObliczDate();
}

void Czas::KolejnyDzien()
{
    Czas::dzien++;
    Czas::ObliczDate();
    Czas::aktualnaData = to_string(Czas::dzienMiesiaca) + "-" + to_string(Czas::miesiacRoku) + "-" + to_string(Czas::rok);
}
void Czas::ObliczDate()
{
    int dzien_temp = Czas::dzien + Czas::poczatkowePrzesuniecieDnia;
    Czas::rok = Czas::rokPoczatkowy + (dzien_temp + 1) / (3*365 + 366);///Oblicza okres co cztery lata by nie pominac dni przestepnych (to wynosi 1461)

    dzien_temp = dzien_temp % (3*365 + 366);
    bool bylPrzestepny = false;
    for(int i=0;i<3;i++)
    {
        if(bylPrzestepny)
        {
            if((dzien_temp >= (i-1) * 365 + 366 - 1) && (dzien_temp <= i * 365 + 366 - 1))
            {
                liczbaDniZwykla[1] = 29;
                Czas::rok += i;
                dzien_temp = (dzien_temp + 1 - 366 % 365);
                break;
            }
        }
        else
        {
            if(dzien_temp >= (i * 365 + 1) && dzien_temp <= ((i+1) * 365))
            {
                liczbaDniZwykla[1] = 28;
                Czas::rok += i;
                if(dzien_temp % 365 != 0)
                    dzien_temp = dzien_temp % 365;
                else
                    dzien_temp = (i+1) * 365;
                break;
            }
        }
        if((Czas::rok - 3 + i) % 4 == 0)
            bylPrzestepny = true;
    }
    for(int i=0,suma_dni=0;i<12;i++)
    {
        suma_dni += liczbaDniZwykla[i];
        if(dzien_temp<=suma_dni)
        {
            Czas::miesiacRoku = i+1;
            Czas::dzienMiesiaca = dzien_temp - suma_dni + liczbaDniZwykla[i];
            break;
        }
    }
}
string Czas::getAktualnaData(bool wypisz)
{
    if(wypisz)
        cout<<Czas::aktualnaData;
    else
        return Czas::aktualnaData;
    return "";
}
int Czas::getDzien(bool wypisz)
{
    if(wypisz)
        cout<<Czas::dzien;
    else
        return Czas::dzien;
    return -1;
}
