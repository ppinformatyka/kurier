#ifndef CZAS_H
#define CZAS_H

#include <iostream>
#include <string>

using namespace std;

//Autor: Rafal Mikrut
class Czas
{
    static int dzien;

    static int dzienMiesiaca;
    static int miesiacRoku;
    static int rok;

    static int liczbaDniZwykla[12];

    static int dzienPoczatkowy;
    static int miesiacPoczatkowy;
    static int rokPoczatkowy;
    static int poczatkowePrzesuniecieDnia;

    static string aktualnaData;

    static void ObliczDate();

    public:
    static string getAktualnaData(bool);
    static int getDzien(bool = false);
    static void KolejnyDzien();
    static void Inicjalizuj();
};

#endif
