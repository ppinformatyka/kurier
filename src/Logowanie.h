#ifndef LOGOWANIE_H
#define LOGOWANIE_H

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>

#include "Kurier.h"
#include "Klient.h"
#include "Administrator.h"
#include "Magazyn.h"
#include "Naglowek.h"

using namespace std;

//Autor: Rafal Mikrut
class Logowanie
{
    static bool zalogowany;
    static string zalogowanyUzytkownik;

    static fstream uchwytDoPlikuDanych;
    static fstream uchwytDoPlikuPrzesylek;
    static fstream uchwytDoPlikuDanychBACKUP;///Bez szyfrowania
    static fstream uchwytDoPlikuPrzesylekBACKUP;///Bez szyfrowania
    ///Elementy potrzebne do szyfrowania
    static string listaZnakow;
    static unsigned int iloscZnakow;
    static unsigned int przesuniecieZnakowSzyfrowanie;
    static unsigned int przesuniecieZnakowDeszyfowanie;

    public:
    static void Inicjalizuj();

    static void* Zaloguj();
    static void Wyloguj();
    static void Rejestruj();

    static bool CzyJestZalogowany();
    static string JakiUzytkownik();

    static void Szyfrowanie(string&);
    static void Deszyfrowanie(string&);
    static int ZnajdzZnak(char);

    static void WczytanieDanych();///Wczytuje dane logowania z pliku do Magazynu
    static void ZapisywanieDanych();///Zapisuje uzytkowikow do pliku(mozna pomyslec o paczkach)

};
#endif
