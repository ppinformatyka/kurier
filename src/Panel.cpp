#include "Panel.h"

Panel::Panel(Kurier* aKurier)
{
    WyswietlPanel(aKurier);
}
void Panel::WyswietlPanel(Kurier* aKurier)
{
    char znak;
    while(true)
    {
        try
        {
            Naglowek::RysujNaglowek(aKurier);
            cout<< "Wybierz opcje:" << endl << endl;

            cout<< "1. Wyswietl przewozone przesylki" << endl;
            cout<< "2. Wywal dowolna przesylke"<<endl;
            cout<< "0. Wyloguj" << endl;

            cin >> znak;
            cin.ignore(INT_MAX,'\n');
            if(znak == '1')WyswietlPrzewozonePrzesylki(aKurier);///Wyswietl przewozone Przesylki
            else if(znak == '2')WywalJakasPrzesylke(aKurier);///Wywal Przesylke
            else if(znak == '0')break;///Wyloguj
        }
        catch(string)
        {
            continue;
        }
    }
}
void Panel::WyswietlPrzewozonePrzesylki(Kurier* aKurier)
{
    Naglowek::RysujNaglowek(aKurier);
    cout<<"Okno Kuriera - Sprawdzanie Przewozonych Przesylek"<<endl<<endl;
    if(!aKurier->getWTrasie())
    {
        cout<<"Kurier nie jest w Trasie!";
        cin.get();
        throw(string());
    }
    else
    {
        Magazyn::WyswietlPrzesylki(aKurier);
        cin.get();
    }
}
void Panel::WywalJakasPrzesylke(Kurier* aKurier)
{
    int id=-1;
    while(true)
    {
        Naglowek::RysujNaglowek(aKurier);
        if(id!=-1)
            cout<<"Wpisales niepoprawne id, Wpisz ponownie"<<endl;
        cout<< "Wpisz id przesylki do wywalenia:" << endl << endl;

        cin >> id;
        cin.ignore(INT_MAX,'\n');
        if(id == -1) throw(string());
        if(Magazyn::WywalJakasPrzesylke(aKurier->getId(),id))
        {
            cout<<endl<<"Wyrzucono przesylke w krzaki";
            cin.get();
            throw(string());
        }
    }
}
/// //////////////////////////////////////////////////////////////////////////////////////////////////////////////ADMINISTRATOR
Panel::Panel(Administrator* aAdministrator)
{
    WyswietlPanel(aAdministrator);
}
void Panel::WyswietlPanel(Administrator* aAdministrator)
{
    char znak;

    while(true)
    {
        try
        {
            Naglowek::RysujNaglowek(aAdministrator);
            cout<< "Wybierz opcje:" << endl << endl;

            cout<< "1. Wyswietl Wszystko" << endl;
            cout<< "2. Usun Uzytkownika" << endl;
            cout<< "0. Wyloguj" << endl;

            cin >> znak;
            cin.ignore(INT_MAX,'\n');
            if(znak == '1')WyswietlWszystko(aAdministrator);///Wyswietl uzytkownikow
            else if(znak == '2')UsunUzytkownika(aAdministrator);///Usun uzytkownika i jego przesylki
            else if(znak == '0')break;///Wyloguj
        }
        catch(string)
        {
            continue;
        }
    }
}
void Panel::UsunUzytkownika(Administrator* aAdministrator)
{
    string login="";
    while(true)
    {
        Naglowek::RysujNaglowek(aAdministrator);
        if(login!="")
            cout<<"Wpisales niepoprawny login, Wpisz ponownie"<<endl;
        cout<< "Wpisz login:" << endl << endl;

        cin >> login;
        cin.ignore(INT_MAX,'\n');
        if(login == "back" || aAdministrator->getLogin() == login) throw(string());
        if(Magazyn::UsunUzytkownika(login))
        {
            cout<<endl<<"Usunieto uzytkownika";
            throw(string());
        }
    }
}
void Panel::WyswietlWszystko(Administrator* aAdministrator)
{
    Naglowek::RysujNaglowek(aAdministrator);
    cout<<"Okno Administratora - Sprawdzanie Wszystkich Przesylek"<<endl<<endl;
    Magazyn::WyswietlWszystko();
    cin.get();
}

/// //////////////////////////////////////////////////////////////////////////////////////////////////////////////KLIENT
Panel::Panel(Klient* aKlient)
{
    WyswietlPanel(aKlient);
}
void Panel::WyslijPrzesylke(Klient* aKlient)
{
    int waga=-1,cena=-1,idOdbiorcy=-1;
    string nazwa,sPTyp,sTTyp;
    PrzesylkaTyp PTyp;
    TransportTyp TTyp;

    do{
        do{/// ////////////////////WAGA
            Naglowek::RysujNaglowek(aKlient);
            cout<<"Okno Wysylania Paczki"<< endl <<endl;
            cout<<"Wpisz Wage: ";
            cin>>waga;
            cin.ignore(INT_MAX,'\n');
            if(waga == -1) throw(string());
        }while(waga<=0||waga>100);
        do{///ID ODBIORCY
            Naglowek::RysujNaglowek(aKlient);
            cout<<"Okno Wysylania Paczki"<< endl <<endl;
            cout<<"Wpisz Wage: " << waga << endl;
            cout<<"Wpisz Id Odbiorcy: ";
            cin>>idOdbiorcy;
            cin.ignore(INT_MAX,'\n');
            if(idOdbiorcy == -1) throw(string());
        }while(!Magazyn::CzyIstniejeKlient(idOdbiorcy) || (idOdbiorcy == aKlient->getId()));
        /// //////////////////////TYP TRANSPORTU
        do{
            Naglowek::RysujNaglowek(aKlient);
            cout<<"Okno Wysylania Paczki"<<endl<<endl;
            cout<<"Wpisz Wage: " <<waga<<endl;
            cout<<"Wpisz Id Odbiorcy: "<<nazwa<<endl;
            cout<<"Wybierz transport paczki: ";
            cout<<"\n\tSzybki\n\tSredni\n\tWolny\n\tNiezdefiniowany:\n";
            cin>>sTTyp;
            cin.ignore(INT_MAX,'\n');
            if(sTTyp == "back") throw(string());
        }while(sTTyp!="Szybki"&&sTTyp!="Sredni"&&sTTyp!="Wolny"&&sTTyp!="Niezdefiniowany");
        if(sTTyp=="Szybki")TTyp = TransportTyp::szybki;
        else if(sTTyp=="Sredni")TTyp = TransportTyp::sredni;
        else if(sTTyp=="Wolny")TTyp = TransportTyp::wolny;
        else if(sTTyp=="Niezdefiniowany")TTyp = TransportTyp::niezdefiniowany;
        /// //////////////////////TYP PRZESYLKI
        do{
            Naglowek::RysujNaglowek(aKlient);
            cout<<"Okno Wysylania Paczki"<<endl<<endl;
            cout<<"Wpisz Wage: " <<waga<<endl;
            cout<<"Wpisz Id Odbiorcy: "<<nazwa<<endl;
            cout<<"Wybierz transport paczki: "<<sTTyp<<endl;
            cout<<"Wybierz zawartosc paczki"<<endl;
            cout<<"\tNiebezpieczna\n\tTlukliwa\n\tCenna\n\tNiezdefiniowana:\n";
            cin>>sPTyp;
            cin.ignore(INT_MAX,'\n');
            if(sPTyp == "back") throw(string());
        }while(sPTyp!="Niebezpieczna"&&sPTyp!="Tlukliwa"&&sPTyp!="Cenna"&&sPTyp!="Niezdefiniowana");
        if(sPTyp=="Niebezpieczna")PTyp = PrzesylkaTyp::niebezpieczna;
        else if(sPTyp=="Tlukliwa")PTyp = PrzesylkaTyp::tlukliwa;
        else if(sPTyp=="Cenna")PTyp = PrzesylkaTyp::cenna;
        else if(sPTyp=="Niezdefiniowana")PTyp = PrzesylkaTyp::niezdefiniowana;
        /// ////////////////////// CENA DTODO
        //obliczanie ceny
        cena = 50; //Jezeli cena jest zbyt mala to wychodzi z funkcji

        if(cena > aKlient->getKasa())
        {
            cout<<"Masz za malo srodkow na koncie. Potrzebujesz "<<cena<<", a masz tylko "<<aKlient->getKasa();
            cin.get();
            continue;
        }
        /// ////////////////////// CZAS JAZDY DTODO

        /// /////////////////////// NAZWA
        cout<<"Wpisz nazwe przesylki: ";
        cin>>nazwa;
        cin.ignore(INT_MAX,'\n');
        if(nazwa == "back") throw(string());

        aKlient->DodajKase(-cena);
        Magazyn::DodajPrzesylke(new Przesylka(waga,cena,Przesylka::getOstatnieId()+1,aKlient->getId(),idOdbiorcy,nazwa,PTyp,Czas::getAktualnaData(false),"0",-1));

        cout<<endl<<endl<<"Pomyslnie wyslano paczke";
        cin.get();
        break;
    }while(true);
}
void Panel::SprawdzWyslanePrzesylki(Klient* aKlient)
{
    Naglowek::RysujNaglowek(aKlient);
    cout<<"Okno Klienta - Sprawdzanie Przesylek"<<endl<<endl;
    cout<<"Rodzaj Przesylki,waga,cena,idPrzesylki,idNadawcy,idOdbiorcy,Nazwa,Typ,DataWyslania,DataDostarczenia,IdKuriera"<<endl<<endl;
    Magazyn::WyswietlPrzesylki(aKlient);
    cin.get();
}
void Panel::WyswietlPanel(Klient* aKlient)
{
    char znak;
    while(true)
    {
        try
        {
            Naglowek::RysujNaglowek(aKlient);
            cout<< "Wybierz opcje:" << endl << endl;

            cout<< "1. Nadaj przesylke" << endl;
            cout<< "2. Sprawdz wyslane przesylki" << endl;
            cout<< "0. Wyloguj" << endl;

            cin >> znak;
            cin.ignore(INT_MAX,'\n');
            if(znak == '1')WyslijPrzesylke(aKlient);///Nadaj przesylke
            else if(znak == '2')SprawdzWyslanePrzesylki(aKlient);///Sprawdz wyslane przesylki
            else if(znak == '0')break;///Wyloguj
        }
        catch(string)
        {
           // continue;
        }
    }
}
