#include "Logowanie.h"

    bool Logowanie::zalogowany;
    string Logowanie::zalogowanyUzytkownik;

    string Logowanie::listaZnakow;
    unsigned int Logowanie::iloscZnakow;
    unsigned int Logowanie::przesuniecieZnakowSzyfrowanie;
    unsigned int Logowanie::przesuniecieZnakowDeszyfowanie;///Zrobione po to by bylo latwo zmienic przesuniencie

    fstream Logowanie::uchwytDoPlikuDanych;
    fstream Logowanie::uchwytDoPlikuPrzesylek;

    fstream Logowanie::uchwytDoPlikuDanychBACKUP;
    fstream Logowanie::uchwytDoPlikuPrzesylekBACKUP;
void Logowanie::Inicjalizuj()
{
    Logowanie::listaZnakow = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890,./;'[]-=<>?\"{}_+\\|";
    Logowanie::iloscZnakow = Logowanie::listaZnakow.size();
    Logowanie::przesuniecieZnakowSzyfrowanie = 0;
    Logowanie::przesuniecieZnakowDeszyfowanie = 0;

    Logowanie::zalogowany = false;
    Logowanie::zalogowanyUzytkownik = "";

    Logowanie::WczytanieDanych();
}
void Logowanie::Rejestruj()
{
    string login="",haslo="",typ="",imie="",nazwisko="";
    int id;
    string poprawne;

    Naglowek::RysujNaglowek(nullptr);
    cout<<"Okno Rejestracji"<<endl<<endl;

    do{

        Naglowek::RysujNaglowek(nullptr);
        cout<<"Okno Rejestracji"<<endl<<endl;
        if(login !="")
            cout<<"Ten login jest juz w uzyciu!!!"<<endl;
        cout<<"Wpisz login: ";
        cin>>login;
        cin.ignore(INT_MAX,'\n');
        if(login == "back") throw(float(0));
    }while(Magazyn::CzyIstniejeUzytkownik(login));

    cout<<"Wpisz haslo: ";
    cin>>haslo;
    cin.ignore(INT_MAX,'\n');
    if(haslo == "back") throw(float(0));
    cout<<"haslo";
    cin.get();

    do{
        Naglowek::RysujNaglowek(nullptr);
        cout<<"Okno Rejestracji"<<endl<<endl;
        if(typ != "")
            cout<<"Wpisales bledny typ"<<endl;
        cout<<"Wpisz login: "<<login<<endl;
        cout<<"Wpisz haslo: "<<haslo<<endl;
        cout<<"Wpisz typ(Kurier,Klient,Administrator): ";//Kurier,Klient,Administator
        cin>>typ;
        cin.ignore(INT_MAX,'\n');
        if(typ == "back") throw(float(0));
    }while(typ != "Kurier" && typ != "Klient" && typ != "Administrator");

    id = Czlowiek::getOstatnieId() + 1;

    cout<<endl<<"Wpisz imie: ";
    cin>>imie;
    cin.ignore(INT_MAX,'\n');
    if(imie == "back") throw(float(0));

    cout<<endl<<"Wpisz nazwisko: ";
    cin>>nazwisko;
    cin.ignore(INT_MAX,'\n');
    if(nazwisko == "back") throw(float(0));

    if(typ == "Administrator")
    {
        Magazyn::DodajAdministratora(new Administrator(login,haslo,id,typ,imie,nazwisko));
    }
    else if(typ == "Kurier")
    {
        Magazyn::DodajKuriera(new Kurier(login,haslo,id,typ,imie,nazwisko,0));
    }
    else if(typ == "Klient")
    {
        Magazyn::DodajKlienta(new Klient(login,haslo,id,typ,imie,nazwisko,200));
    }

}
int Logowanie::ZnajdzZnak(char znak)
{
    for(unsigned int i=0;i<iloscZnakow;i++)
        if(znak == listaZnakow[i])
            return i;
    return -1;
}
void Logowanie::Szyfrowanie(string& tekst)
{
    for(unsigned int i=0;i<tekst.size();i++)
    {
        if (tekst[i] != ' ')
            tekst[i] = listaZnakow[(ZnajdzZnak(tekst[i]) + przesuniecieZnakowSzyfrowanie) % iloscZnakow];
    };
}
void Logowanie::Deszyfrowanie(string& tekst)
{
    for(unsigned int i=0;i<tekst.size();i++)
    {
        if (tekst[i] != ' ')
            tekst[i] =listaZnakow[(ZnajdzZnak(tekst[i]) - przesuniecieZnakowDeszyfowanie + iloscZnakow) % iloscZnakow];
    };
}
void Logowanie::WczytanieDanych()
{
    uchwytDoPlikuDanych.open("dane/DaneLogowania",ios::in);
    uchwytDoPlikuPrzesylek.open("dane/DanePrzesylek",ios::in);
    string loginWczytany="";
    string hasloWczytane="";
    int idWczytane=-1;
    string sIdWczytane="";
    string typWczytany="";
    string imieWczytane="";
    string nazwiskoWczytane="";
    int dodatekWczytany = -1;///Kasa w Kliencie i czasDoZakonczenia podrozy w Kurierze
    string sDodatekWczytany = "";

    while(!uchwytDoPlikuDanych.eof())///Wczytywanie uzytkownikow
    {
        uchwytDoPlikuDanych >> loginWczytany;
        Deszyfrowanie(loginWczytany);
        uchwytDoPlikuDanych >> hasloWczytane;
        Deszyfrowanie(hasloWczytane);
        uchwytDoPlikuDanych >> sIdWczytane;
        Deszyfrowanie(sIdWczytane);
        idWczytane = stoi(sIdWczytane);
        uchwytDoPlikuDanych >> typWczytany;
        Deszyfrowanie(typWczytany);
        uchwytDoPlikuDanych >> imieWczytane;
        Deszyfrowanie(imieWczytane);
        uchwytDoPlikuDanych >> nazwiskoWczytane;
        Deszyfrowanie(nazwiskoWczytane);
        uchwytDoPlikuDanych >> sDodatekWczytany;
        Deszyfrowanie(sDodatekWczytany);
        dodatekWczytany = stoi(sDodatekWczytany);

        if(typWczytany == "Administrator")
        {
            Magazyn::DodajAdministratora(new Administrator(loginWczytany,hasloWczytane,idWczytane,typWczytany,imieWczytane,nazwiskoWczytane));
        }
        else if(typWczytany == "Kurier")
        {
            Magazyn::DodajKuriera(new Kurier(loginWczytany,hasloWczytane,idWczytane,typWczytany,imieWczytane,nazwiskoWczytane,dodatekWczytany));
        }
        else if(typWczytany == "Klient")
        {
            Magazyn::DodajKlienta(new Klient(loginWczytany,hasloWczytane,idWczytane,typWczytany,imieWczytane,nazwiskoWczytane,dodatekWczytany));
        }
    }
    string rodzajPrzesylki="";
    int pWaga=0;
    string sWaga="";
    int pIdPPP=0;
    string sIdPPP="";
    int pCena=0;
    string sCena="";
    int pIdNadawcy=0;
    string sIdNadawcy="";
    int pIdOdbiorcy=0;
    string sIdOdbiorcy="";
    string sNazwa="";
    int pTyp=0;
    string sTyp="";
    string sDataWyslania="0";
    string sDataDostarczenia="0";
    int pIdKuriera=0;
    string sIdKuriera="";
    while(!uchwytDoPlikuPrzesylek.eof())///Wczytywanie Przesylek
    {
        uchwytDoPlikuPrzesylek >> rodzajPrzesylki;
        Deszyfrowanie(rodzajPrzesylki);

        uchwytDoPlikuPrzesylek >> sWaga;
        Deszyfrowanie(sWaga);
        pWaga = stoi(sWaga);

        uchwytDoPlikuPrzesylek >> sCena;
        Deszyfrowanie(sCena);
        pCena = stoi(sCena);

        uchwytDoPlikuPrzesylek >> sIdPPP;
        Deszyfrowanie(sIdPPP);
        pIdPPP = stoi(sIdPPP);

        uchwytDoPlikuPrzesylek >> sIdNadawcy;
        Deszyfrowanie(sIdNadawcy);
        pIdNadawcy = stoi(sIdNadawcy);

        uchwytDoPlikuPrzesylek >> sIdOdbiorcy;
        Deszyfrowanie(sIdOdbiorcy);
        pIdOdbiorcy = stoi(sIdOdbiorcy);

        uchwytDoPlikuPrzesylek >> sNazwa;
        Deszyfrowanie(sNazwa);

        uchwytDoPlikuPrzesylek >> sTyp;
        Deszyfrowanie(sTyp);
        pTyp = stoi(sTyp);

        uchwytDoPlikuPrzesylek >> sDataWyslania;
        Deszyfrowanie(sDataWyslania);

        uchwytDoPlikuPrzesylek >> sDataDostarczenia;
        Deszyfrowanie(sDataDostarczenia);

        uchwytDoPlikuPrzesylek >> sIdKuriera;
        Deszyfrowanie(sIdKuriera);
        pIdKuriera = stoi(sIdKuriera);

        /*cout<<pWaga<<"\t"<<pCena<<"\t"<<pIdPPP<<"\t"<<pIdNadawcy<<"\t"<<pIdOdbiorcy<<"\t"<<sNazwa<<"\t"<<pTyp<<"\t"<<sDataWyslania<<"\t"<<sDataDostarczenia<<"\t"<<pIdKuriera<<endl;
        cin.get();*/

        if(rodzajPrzesylki == "Przesylka")
        {
            Magazyn::DodajPrzesylke(new Przesylka(pWaga,pCena,pIdPPP,pIdNadawcy,pIdOdbiorcy,sNazwa,static_cast<PrzesylkaTyp>(pTyp),sDataWyslania,sDataDostarczenia,pIdKuriera));
        }
        else if(rodzajPrzesylki == "PrzesylkaDostarczona")
        {
            Magazyn::DodajPrzesylkeDostarczona(new Przesylka(pWaga,pCena,pIdPPP,pIdNadawcy,pIdOdbiorcy,sNazwa,static_cast<PrzesylkaTyp>(pTyp),sDataWyslania,sDataDostarczenia,pIdKuriera));
        }
        else if(rodzajPrzesylki == "PrzesylkaPrzewozona")
        {
            Magazyn::DodajPrzesylkePrzewozona(new Przesylka(pWaga,pCena,pIdPPP,pIdNadawcy,pIdOdbiorcy,sNazwa,static_cast<PrzesylkaTyp>(pTyp),sDataWyslania,sDataDostarczenia,pIdKuriera));
        }
        else
        {
            cout<<"Blad przy wczytywaniu paczek - zly rodzaj Przesylki";
            cin.get();
        }
    }
    Magazyn::PrzeliczWagePrzesylekKurierow();
}

void Logowanie::ZapisywanieDanych()
{
    uchwytDoPlikuDanych.close();
    uchwytDoPlikuPrzesylek.close();
    uchwytDoPlikuDanych.open("dane/DaneLogowania",ios::out|ios::trunc);
    uchwytDoPlikuPrzesylek.open("dane/DanePrzesylek",ios::out|ios::trunc);
    uchwytDoPlikuDanychBACKUP.open("dane/DaneLogowaniaBACKUP",ios::out|ios::trunc);
    uchwytDoPlikuPrzesylekBACKUP.open("dane/DanePrzesylekBACKUP",ios::out|ios::trunc);

    Czlowiek* zapisywanyCzlowiek;



    bool poczatekKuriera = true;
    bool poczatekKlienta = true;
    bool poczatekAdministratora = true;

    bool pierwszaPrzesylka = true;

    string sLogin;
    string sHaslo;
    int pId;
    string sId;
    string sTypPrzesylki;
    string sImie;
    string sNazwisko;
    int pDodatekWczytany = 0;
    string sDodatekWczytany;
    while(true)///Zapisywanie uzytkownikow
    {
        Magazyn::setPoczatek(poczatekKuriera);
        Kurier* temp = Magazyn::ZwrocKuriera();
        if(temp != nullptr)
            pDodatekWczytany = temp->getCzasDoZakonczeniaPodrozy();
        zapisywanyCzlowiek = reinterpret_cast<Czlowiek*>(temp);

        poczatekKuriera = false;

        if(zapisywanyCzlowiek == nullptr)
        {
            Magazyn::setPoczatek(poczatekKlienta);
            Klient* temp = Magazyn::ZwrocKlienta();
            if(temp != nullptr)
                pDodatekWczytany = temp->getKasa();
            zapisywanyCzlowiek = reinterpret_cast<Czlowiek*>(temp);
            poczatekKlienta = false;
        }
        if(zapisywanyCzlowiek == nullptr)
        {
            Magazyn::setPoczatek(poczatekAdministratora);
            zapisywanyCzlowiek = reinterpret_cast<Czlowiek*>(Magazyn::ZwrocAdministratora());
            poczatekAdministratora = false;
        }
        if(zapisywanyCzlowiek == nullptr)
        {
            break;
        }

        if(pierwszaPrzesylka != true)
        {
            uchwytDoPlikuDanych<<endl;
            uchwytDoPlikuDanychBACKUP<<endl;
        }
        pierwszaPrzesylka = false;

        sLogin = zapisywanyCzlowiek->getLogin();
        sHaslo = zapisywanyCzlowiek->getHaslo();
        pId = zapisywanyCzlowiek->getId();
        sId = to_string(pId);
        sTypPrzesylki = zapisywanyCzlowiek->getTyp();
        sImie = zapisywanyCzlowiek->getImie();
        sNazwisko = zapisywanyCzlowiek->getNazwisko();
        sDodatekWczytany = to_string(pDodatekWczytany);


        uchwytDoPlikuDanychBACKUP << sLogin << " ";
        Szyfrowanie(sLogin);
        uchwytDoPlikuDanych << sLogin << " ";

        uchwytDoPlikuDanychBACKUP << sHaslo << " ";
        Szyfrowanie(sHaslo);
        uchwytDoPlikuDanych << sHaslo << " ";

        uchwytDoPlikuDanychBACKUP << pId << " ";
        Szyfrowanie(sId);
        uchwytDoPlikuDanych << sId << " ";

        uchwytDoPlikuDanychBACKUP << sTypPrzesylki << " ";
        Szyfrowanie(sTypPrzesylki);
        uchwytDoPlikuDanych << sTypPrzesylki << " ";

        uchwytDoPlikuDanychBACKUP << sImie << " ";
        Szyfrowanie(sImie);
        uchwytDoPlikuDanych << sImie << " ";

        uchwytDoPlikuDanychBACKUP << sNazwisko << " ";
        Szyfrowanie(sNazwisko);
        uchwytDoPlikuDanych << sNazwisko << " ";

        uchwytDoPlikuDanychBACKUP << sDodatekWczytany;
        Szyfrowanie(sDodatekWczytany);
        uchwytDoPlikuDanych << sDodatekWczytany;
    }
    Przesylka* zapisywanaPrzesylka;
    bool poczatekPrzesylki = true;
    bool poczatekPrzesylkiPrzewozonych = true;
    bool poczatekPrzesylkiDostarczonych = true;

    pierwszaPrzesylka = true;

    string rodzajPrzesylki;
    int pWaga;
    string sWaga;
    int pCena;
    string sCena;
    int pIdPPP;
    string sIdPPP;
    int pIdNadawcy;
    string sIdNadawcy;
    int pIdOdbiorcy;
    string sIdOdbiorcy;
    string sNazwa;
    int pTyp;
    string sTyp;
    string sDataWyslania;
    string sDataDostarczenia;
    int pIdKuriera;
    string sIdKuriera;

    while(true)///Zapisywanie Przesylek
    {
        Magazyn::setPoczatek(poczatekPrzesylki);
        zapisywanaPrzesylka = reinterpret_cast<Przesylka*>(Magazyn::ZwrocPrzesylke());
        poczatekPrzesylki = false;
        rodzajPrzesylki = "Przesylka";
        if(zapisywanaPrzesylka == nullptr)
        {
            Magazyn::setPoczatek(poczatekPrzesylkiPrzewozonych);
            zapisywanaPrzesylka = reinterpret_cast<Przesylka*>(Magazyn::ZwrocPrzesylkePrzewozona());
            poczatekPrzesylkiPrzewozonych = false;
            rodzajPrzesylki = "PrzesylkaPrzewozona";
        }
        if(zapisywanaPrzesylka == nullptr)
        {
            Magazyn::setPoczatek(poczatekPrzesylkiDostarczonych);
            zapisywanaPrzesylka = reinterpret_cast<Przesylka*>(Magazyn::ZwrocPrzesylkeDostarczona());
            poczatekPrzesylkiDostarczonych = false;
            rodzajPrzesylki = "PrzesylkaDostarczona";
        }
        if(zapisywanaPrzesylka == nullptr)
            break;

        if(pierwszaPrzesylka != true)
        {
            uchwytDoPlikuPrzesylek<<endl;
            uchwytDoPlikuPrzesylekBACKUP<<endl;
        }
        pierwszaPrzesylka = false;


        pWaga = zapisywanaPrzesylka->getWaga();
        pCena = zapisywanaPrzesylka->getCena();
        pIdPPP = zapisywanaPrzesylka->getId();
        pIdNadawcy = zapisywanaPrzesylka->getIdNadawcy();
        pIdOdbiorcy = zapisywanaPrzesylka->getIdOdbiorcy();
        pTyp = static_cast<int>(zapisywanaPrzesylka->getTyp(true));
        pIdKuriera = zapisywanaPrzesylka->getIdKuriera();

        sWaga = to_string(pWaga);
        sCena = to_string(pCena);
        sIdPPP = to_string(pIdPPP);
        sIdNadawcy = to_string(pIdNadawcy);
        sIdOdbiorcy = to_string(pIdOdbiorcy);
        sNazwa = zapisywanaPrzesylka->getNazwa();
        sTyp = to_string(pTyp);
        sDataWyslania = zapisywanaPrzesylka->getDataWyslania();
        sDataDostarczenia = zapisywanaPrzesylka->getDataDostarczenia();
        sIdKuriera = to_string(pIdKuriera);

        uchwytDoPlikuPrzesylekBACKUP << rodzajPrzesylki << " ";
        Szyfrowanie(rodzajPrzesylki);
        uchwytDoPlikuPrzesylek << rodzajPrzesylki << " ";

        uchwytDoPlikuPrzesylekBACKUP << pWaga << " ";
        Szyfrowanie(sWaga);
        uchwytDoPlikuPrzesylek << sWaga << " ";

        uchwytDoPlikuPrzesylekBACKUP << pCena << " ";
        Szyfrowanie(sCena);
        uchwytDoPlikuPrzesylek << sCena << " ";

        uchwytDoPlikuPrzesylekBACKUP << pIdPPP << " ";
        Szyfrowanie(sIdPPP);
        uchwytDoPlikuPrzesylek << sIdPPP << " ";

        uchwytDoPlikuPrzesylekBACKUP << pIdNadawcy << " ";
        Szyfrowanie(sIdNadawcy);
        uchwytDoPlikuPrzesylek << sIdNadawcy << " ";

        uchwytDoPlikuPrzesylekBACKUP << pIdOdbiorcy << " ";
        Szyfrowanie(sIdOdbiorcy);
        uchwytDoPlikuPrzesylek << sIdOdbiorcy << " ";

        uchwytDoPlikuPrzesylekBACKUP << sNazwa << " ";
        Szyfrowanie(sNazwa);
        uchwytDoPlikuPrzesylek << sNazwa << " ";

        uchwytDoPlikuPrzesylekBACKUP << pTyp << " ";
        Szyfrowanie(sTyp);
        uchwytDoPlikuPrzesylek << sTyp << " ";

        uchwytDoPlikuPrzesylekBACKUP << sDataWyslania << " ";
        Szyfrowanie(sDataWyslania);
        uchwytDoPlikuPrzesylek << sDataWyslania << " ";

        uchwytDoPlikuPrzesylekBACKUP << sDataDostarczenia << " ";
        Szyfrowanie(sDataDostarczenia);
        uchwytDoPlikuPrzesylek << sDataDostarczenia << " ";

        uchwytDoPlikuPrzesylekBACKUP << pIdKuriera;
        Szyfrowanie(sIdKuriera);
        uchwytDoPlikuPrzesylek << sIdKuriera;
    }
}
void* Logowanie::Zaloguj()
{
    void* zalogowanyCzlowiek;
    string login="";
    string haslo="";

    string wyszukaneHaslo;
    string wyszukanyZawod;

    do{
        Naglowek::RysujNaglowek(nullptr);
        if(login != "")
            cout<<"Niepoprawny Login Sprobuj Ponownie"<<endl<<endl;

        cout<<"Okno Logowania "<<endl<<endl;
        cout<<"Wpisz Login: ";
        cin>>login;
        cin.ignore(INT_MAX,'\n');

        wyszukaneHaslo = Magazyn::WyszukajHasloUzytkownika(login);
        wyszukanyZawod = Magazyn::WyszukajZawodUzytkownika(login);

        //if(login == "exit") throw(int(0));
        if(login == "back") throw(float(0));
    }while(wyszukaneHaslo == "");

    Naglowek::RysujNaglowek(nullptr);

    do{
        cout<<"Okno Logowania "<<endl<<endl;
        cout<<"Wpisz Login: "<<login<<endl;
        cout<<"Wpisz Haslo: ";
        cin>>haslo;
        cin.ignore(INT_MAX,'\n');

        //if(haslo == "exit") throw(int(0));
        if(login == "back") throw(float(0));

        Naglowek::RysujNaglowek(nullptr);
        if(haslo != wyszukaneHaslo)
            cout<<"Niepoprawne Haslo Sprobuj Ponownie"<<endl<<endl;
        else
            break;
    }while(true);

    zalogowanyUzytkownik = wyszukanyZawod;
    zalogowany = true;
    zalogowanyCzlowiek = Magazyn::ZwrocSzukanegoUzytkownika(login);
    return zalogowanyCzlowiek;
}
void Logowanie::Wyloguj()
{
    zalogowany = false;
}
bool Logowanie::CzyJestZalogowany()
{
    return zalogowany;
}
string Logowanie::JakiUzytkownik()
{
    return zalogowanyUzytkownik;
}
