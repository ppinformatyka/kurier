#ifndef CZLOWIEK_H
#define CZLOWIEK_H

#include <iostream>
#include <string>

using namespace std;

//Autor: Rafal Mikrut
class Czlowiek
{
public:
    Czlowiek(const string&,const string&,const int&,const string&,const string&,const string&);
    void setLogin(const string&);
    void setHaslo(const string&);
    void setId(const int&);
    void setTyp(const string&);
    void setImie(const string&);
    void setNazwisko(const string&);

    string getLogin();
    string getHaslo();
    int getId();
    string getTyp();
    string getImie();
    string getNazwisko();

    bool operator==(Czlowiek);

    static int getOstatnieId();
    static void setOstatnieId(int);

    virtual void WypiszDane();

protected:
    string login,haslo;
    int id;
    string typ;
    string imie,nazwisko;
private:
    static int ostatnieId;
};

#endif
