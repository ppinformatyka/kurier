
/*
 * Trasa.cpp
 * Created on: 31 maj 2018
 * Author: Dawid Sobolewski
 */

#include "Trasa.h"


string Trasa::getZrodlo() const {
    return this->zrodlo;
}

string Trasa::getCel() const {
    return this->cel;
}

unsigned int Trasa::getOdleglosc() const {
    return this->odleglosc;
}

unsigned int Trasa::getCalkowitaCena() const {
    return this->odleglosc * this->cenaZaKm;
}


unsigned int Trasa::getCzasTransportu() const {
    if(this->odleglosc <= 100) {
        return 1;
    }
    else if(this->odleglosc <= 200) {
        return 2;
    }
    else if(this->odleglosc <= 300) {
        return 3;
    }
    else {
        return 4;
    }
}



string Trasa::getTransportTyp() const {
    switch(typ) {
        case TransportTyp::szybki:
            return string("Fast");
        case TransportTyp::sredni:
            return string("Medium");
        case TransportTyp::wolny:
            return string("Slow");
        case TransportTyp::niezdefiniowany:
            return string("Niezdefiniowany");
        default:
            return string("No Info.");
    }
}

void Trasa::setZrodlo(const string& zrodlo) {
    this->zrodlo = zrodlo;
}

void Trasa::setCel(const string& cel) {
    this->cel = cel;
}

float Trasa::getCalkowitaCenaZPrzesylka(const Przesylka& przesylka) const {
     float temp = this->getCalkowitaCena();

        temp *= ((float)przesylka.getWaga() / 2.0);
        temp *= ((float)przesylka.getCena() / 5.0);

        if(przesylka.getTyp() == "niebezpieczna") {
            temp *= 1.2;
        }
        else if(przesylka.getTyp() == "tlukliwa") {
            temp *= 2.3;
        }
        else {
            temp *= 3.4;
        }

        if(this->getTransportTyp() == "szybki") {
            temp *= 3;
        }
        else if(this->getTransportTyp() == "wolny") {
            temp /= 1.5;
        }

        return temp / 10000;
}

void Trasa::setOdleglosc(int odleglosc) {
    this->odleglosc = odleglosc;
}
