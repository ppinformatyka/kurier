#ifndef POZYCJA_H
#define POZYCJA_H

#include <string>

using namespace std;

//Autor: Dawid Sobolewski
class Pozycja {
public:
    Pozycja(): szerokosc(0), wysokosc(0) {}
    Pozycja(const int szerokosc, const int wysokosc): szerokosc(szerokosc), wysokosc(wysokosc) {}
    ~Pozycja() {}
    string getPozycja() const;
    int getSzerokosc() const;
    int getWysokosc() const;

private:
    int szerokosc;
    int wysokosc;
};

#endif
