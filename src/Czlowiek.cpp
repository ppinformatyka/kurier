#include "Czlowiek.h"

int Czlowiek::ostatnieId = 0;

Czlowiek::Czlowiek(const string& login,const string& haslo,const int& id, const string& typ, const string& imie,const string& nazwisko)
{
    this->login = login;
    this->haslo = haslo;
    this->id = id;
    this->typ = typ;
    this->imie = imie;
    this->nazwisko = nazwisko;
}
string Czlowiek::getLogin()
{
    return login;
}
string Czlowiek::getHaslo()
{
    return haslo;
}
int Czlowiek::getId()
{
    return id;
}
string Czlowiek::getTyp()
{
    return typ;
}
string Czlowiek::getImie()
{
    return imie;
}
string Czlowiek::getNazwisko()
{
    return nazwisko;
}
void Czlowiek::setLogin(const string& login)
{
    this->login=login;
}
void Czlowiek::setHaslo(const string& haslo)
{
    this->haslo=haslo;
}
void Czlowiek::setId(const int& id)
{
    this->id=id;
}
void Czlowiek::setTyp(const string& typ)
{
    this->typ=typ;
}
void Czlowiek::setImie(const string& imie)
{
    this->imie=imie;
}
void Czlowiek::setNazwisko(const string& nazwisko)
{
    this->nazwisko=nazwisko;
}
void Czlowiek::WypiszDane()
{
    cout<<login+"\t"+haslo+"\t"+to_string(id)+"\t"+typ+"\t"+imie+"\t"+nazwisko<<endl;
}

bool Czlowiek::operator==(Czlowiek czlowiekPorownywany)
{
    if(this->getId() == czlowiekPorownywany.getId())
        return true;

    return false;
}

int Czlowiek::getOstatnieId()
{
    return ostatnieId;
}
void Czlowiek::setOstatnieId(int _ostatnieId)
{
    ostatnieId = _ostatnieId;
}
