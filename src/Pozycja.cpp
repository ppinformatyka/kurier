#include "Pozycja.h"

string Pozycja::getPozycja() const {
    string ss = this->szerokosc + " " + this->wysokosc;
    return ss;
}

int Pozycja::getSzerokosc() const {
    return this->szerokosc;
}

int Pozycja::getWysokosc() const {
    return this->wysokosc;
}
