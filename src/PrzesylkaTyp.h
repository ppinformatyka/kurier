#ifndef PRZESYLKATYP_H
#define PRZESYLKATYP_H

//Autor Dawid Sobolewski
enum class PrzesylkaTyp {
     niebezpieczna, tlukliwa, cenna, niezdefiniowana
};

#endif
