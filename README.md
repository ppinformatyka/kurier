CodeBlocks - https://sourceforge.net/projects/codeblocks/files/Binaries/17.12/Windows/codeblocks-17.12mingw-nosetup.zip/download  
Appimage https://bintray.com/probono/AppImages/Code::Blocks_IDE#files
Git - https://git-scm.com/download/win     
     
////////////////////////////////////////////////////////     
Klasy wykonane     
Rafał - Czas, Lista i PrLista, Logowanie(Częściowo), Człowiek   
DawidS -     
          
////////////////////////////////////////////////////////     
STYL PISANIA     
     
WSZYSTKO - Nie stosujemy polskich znaków w innych plikach niż README     
Nazwy Funkcji - Każde słowo zaczyna się od dużej litery a kolejne litery są małe np. TaFunkcjaJest()     
Nazwy Zmiennej - Wszystkie litery są małe oprócz pierwszej litery drugiego i kolejnego słowa np. toJestZmienna     
Nazwa Klas - Konwencja jest taka jak w Funkcjach np. NazwaKlasy     
     
W README.md na końcu linii postawić 5 spacji by być pewnym by dobrze wyglądał ten plik w Bitbucket     
     
////////////////////////////////////////////////////////     
LEGENDA:     
- Z - Zmienna     
- F – Funkcja     
- G - Funkcja Get     
- S - Funkcja Set     
     
     
Funkcja Main     
-Zarządzanie logowaniem     
-Opcja Rejestracji     
-Przechodzenie do kolejnych dni     
     
Klasa Administrator     
- F - Dodaj i Usuń Użytkownika(poprzez ID) - Korzysta z funkcji z Magazyn     
- F - Wyświetlanie użytkowników (np. po 10 z możliwość cofania się)     
- F - Wyświetl Panel - Do wybierania opcji     
- F - Przeciążenie operatora ==(Możliwe że da się to zrobić za pomocą Klasy Człowiek dla wszystkich) zwracjące true gdy (Administrator,int) gdzie int to id Administratora  , (Administrator,Administrator) gdy mają takie samo id - reszta nie musi się zgadzać     
Klasa Czas(powinna wyświetlać się w trybie 01-01-)     
- Z G - Aktualna Data     
- Z G - Aktualny Dzień     
- F - Czas za ... dni ?     
- F - Różnica pomiędzy ... a ... ?     
- Z - Dni, Miesiące, Rok początkowy     
- Z - Początkowe przesuniecie dnia     
- Z - Lista dni z każdego miesiąca     
     
Klasa Człowiek     
- Z G - Login, Hasło     
- Z G - Id - Będzie to jako string by funkcja szyfrująca mogła bez problemów i przeciążeń pracować na tych elementach     
- Z G - Typ     
- Z G – Imię, Nazwisko     
     
Klasa Klient – pochodna człowiek     
- F - Wyślij przesyłkę     
- F – Zwróć listę wysłanych przesyłek(Odczytywana za każdym razem z Magazynu)     
- F - Wyświetl Panel - Do wybierania opcji     
- F - Przeciążenie operatora ==(Możliwe że da się to zrobić za pomocą Klasy Człowiek dla wszystkich) zwracjące true gdy (Klient,int) gdzie int to id Klienta  , (Klient,Klient) gdy mają takie samo id - reszta nie musi się zgadzać, (Klient,Przesyłka) gdy przesyłka jest nadana przez tego Klienta     
     
Klasa Kurier – pochodna człowiek     
- Z – Czas do zakończenia podróży z paczką     
- Z - Pojemność samochodu(waga)     
- Z G S - Stan Kuriera(w trasie z szczegółami, wolny)     
- F - Wyświetl Panel - Do wybierania opcji     
- F - Wyświetlenie przewożonych przesyłek     
- F - Przeciążenie operatora ==(Możliwe że da się to zrobić za pomocą Klasy Człowiek dla wszystkich) zwracjące true gdy (Kurier,int) gdzie int to id Kuriera  , (Kurier,Kurier) gdy mają takie samo id - reszta nie musi się zgadzać, (Kurier,Przesyłka) gdy przesyłka jest przewożona przez tego kuriera     
     
Klasa Lista - Szablon Klasy który zarządza elementami listy PrLista     
- Z - Początek i koniec listy     
- Z G - Długość     
- F – Dodaj na końcu – dodaje element na końcu listy     
- F - Usuń element z końca listy     
- F - Usuń konkretny element (raczej będzie wyszukiwanie po wartości albo adresie szukanego obiektu)     
     
Klasa Logowanie     
- Z G – Zalogowany     
- Z G - Typ zalogowanego użytkownika(Kurier, Klient, Administrator)     
- Z – Uchwyt do pliku z danymi (Hasła/Loginy)     
- Z - Ilość i lista znaków potrzebna do szyfrowania i deszyfrowania danych(powinna być rożna od abcde itp. ze względów bezpieczeństwa)     
- F – Inicjalizuj - Plik jest odszyfrowywany, otwierany i zapisywany na uchwycie     
- F – Zaloguj(Odczytaj dane z pliku czy się zgadzają)     
- F – Wyloguj     
- F - Rejestruj - Rejestruje użytkownika     
- F - Szyfrowanie, Deszyfrowanie i znajdowanie znaku z listy znaków     
- F - Zapisywanie danych - Zapisywanie danych o użytkownikach i ich szyfrowanie i zapisywane do pliku     
     
Klasa Magazyn     
- Z G S - Lista Kurierów     
- Z G S - Lista Klientów     
- Z G S - Lista Administratorów     
- Z G S – Lista Przesyłek     
- Z G S – Lista Przesyłek Przewożonych (Które są przypisane kurierom ale nie są)     
- Z G S – Lista Przesyłek Dostarczonych (By nie siedziały bez celu w zwykłych przesyłkach)     
- F – Przydzielanie paczek kurierom i samochodom     
- F - Dodaj Kuriera, Klienta, Administratora, Przesyłkę, Przesyłkę dostarczoną     
     
Klasa Nagłówek     
- F – Rysuj Nagłówek     
     
Klasa PrLista     
- Z G - Poprzedni i następny - zawiera wskaźniki do innych elementów listy     
- Z G - Wartość - wartość przechowywana przez listę     
- F - Ustaw Aktualny - ustawia aktualną wartość elementu     
- F - Ustaw Poprzednią, Następną listę - zwraca wskaźnik do kolejnych elementów listy     
     
Klasa Przesyłka     
- Z - Waga     
- Z - Id Klienta     
- Z - Id Kuriera przewożącego towar     
- Z - Miejsce Docelowe     
     
////////////////////////////////////////////////////////     
Elementy dodatkowe które możemy zrobić ale mogą być:     
-Gubienie paczek     
-Zyski kurierów ze względu na to jakie przesyłki przewożą     
-Zatrudnianie nowych kurierów i dodawanie nowych klientów     
     
Ogólne rozmyślenia:     
- Program powinien składać się z głównej pętli i każdy jej obieg wyznaczałby jeden dzień     
- Trzeba stworzyć .gitignore na IDE które wybierzemy     
- W jakim języku będziemy tworzyć polski czy angielski?     
- W jaki sposób nazywamy zmienne, funkcje itp.?